import { Injectable } from '@angular/core';
import { User } from './user';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class TestService {

  url ='http://localhost:3000/todos';

  userList: User[] = [
    {userId:101, userName:"Johnny",phone:"9632587410"},
    {userId:102, userName:"benten",phone:"9632587413"},
    {userId:103, userName:"bheem",phone:"9632587413"}
  ]
   
  
  

  constructor(private httpObj:Http) { }

  

  getData() {
    return this.userList;
  }

  showtodo(){
    return this.httpObj.get("http://localhost:3000/todos").pipe(map(res => res.json()));
  }
  viewtodo(id){
    return this.httpObj.get("http://localhost:3000/todos/" +id).pipe(map(res => res.json()));
  
  }

  savetodo(newtodo){
    return this.httpObj.post("http://localhost:3000/todos",newtodo).pipe(map(res => res.json()));
  }
  deltodo(id){
    return this.httpObj.delete("http://localhost:3000/todos/"+id).pipe(map(res => res.json()));
  
    
  }
  updatetodo(id, newtodo){
    return this.httpObj.put("http://localhost:3000/todos/" +id,newtodo).pipe(map(res => res.json()));
  }
}