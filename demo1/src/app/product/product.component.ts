import { Component, OnInit } from '@angular/core';
import { TestService } from '../test.service';
import {User} from '../user';
import { escapeIdentifier } from '@angular/compiler/src/output/abstract_emitter';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  name = 'abs';
  strDate = '11/12/2019';
  users: User[] = [];
  todo:any[] =[];
  mytodo: string;
  show=false;
  showdiv=false;
  tid: string;
  tit: string;

  constructor(private service:TestService) { }

  ngOnInit() {
  }

  showuser(){
    this.users = this.service.getData();
  }

  showtodo()
  {
    this.service.showtodo().subscribe(
      (res) => {
       
        console.log(res);
        this.todo = res;
      },
      (err) => {
       
        console.log(err);
        
        this.todo = err;
      }
    );
  }
  
  viewtodo()
  {
    let  id = this.mytodo;
    this.service.viewtodo(id).subscribe(
      (res) => {
       
        console.log(res);
        this.tit=(res.title);
        // this.todo = res;
      },
      (err) => {
       
        console.log(err);
        
        // this.todo = err;
      }
    );
  }

  addtodo()
  {
    const newtodo = {
      title: this.mytodo
      
    
    };
  
  // savetodo()
  
    this.service.savetodo(newtodo).subscribe(
      (res) => {
        this.show=true;
        this.showdiv=true;
        console.log(res);
        // this.todo = res;
      },
      (err) => {
        this.show=false;
        this.showdiv=true;
        console.log(err);
      }
    );
  }
  deletetodo(id) {
    this.service.deltodo(id).subscribe(
      (res) => {
        console.log(res);
        this.showtodo();
        alert('Todo Deleted');
        // this.todo = res;
      },
      (err) => {
        console.log(err);
      }

    );
  }


edittodo(todo){
  this.tid =todo.id;
  this.mytodo = todo.title;

}

edit()
{
  const newtodo = {
    title:this.mytodo
  };
  this.service.updatetodo(this.tid, newtodo).subscribe(
    (res)=> {
      this.showtodo();
    },
    (err)=> 
    {
      console.log(err);
    }
  );
}
}
 

    

  


